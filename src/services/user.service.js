import { ApiService } from "./api.service";

const userService = {
    login: ({ email, password }) => {
        return new Promise(function(resolve, reject) {
            ApiService.post("/oauth/token", {
                email: email,
                password: password,
                client_id: 3,
            })
                .then(async (res) => {
                    let token = res.data.token;
                    resolve(token);
                })
                .catch((error) => {
                    reject(error.response.data);
                });
        });
    },
    register: async (data) => {
        return await ApiService.post("/oauth/onboard/add", data)
            .then(async (res) => {
                return Promise.resolve(res.data.data);
            })
            .catch((error) => {
                return Promise.reject(error.response.data.error);
            });
    },
};

export { userService };
