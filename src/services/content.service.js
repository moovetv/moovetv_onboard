import { ApiService } from "./api.service";

const contentService = {
    get: ({ token }) => {
        return new Promise(function(resolve, reject) {
            ApiService.get("/taxi/latest", {})
                .setHeader(token)
                .then(async res => {
                    let data = res.data;
                    resolve(data);
                })
                .catch(error => {
                    reject(error.response.data);
                });
        });
    }
};

export { contentService };